Supports the various sensors delivered by UDOO Neo. Reads the values from the streams of the various devices. Note that several devices need to be enabled before they will supply readings.

Licensed under Apache License Version 2 (https://www.apache.org/licenses/LICENSE-2.0)