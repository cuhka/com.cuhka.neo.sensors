package com.cuhka.neo.sensor;

public interface DataIO {
	void write(String to, String value);
	default void write(String to, boolean value) { write(to, value ? "1" : "0"); }
	default void write(String to, long value) { write(to, Long.toString(value)); }
	
	String read(String from);
	default double readDouble(String from) { return Double.parseDouble(read(from)); }
	default boolean readBoolean(String from) { return read(from).equals("1"); }
	default long readLong(String from) { return Long.parseLong(read(from)); }
}
