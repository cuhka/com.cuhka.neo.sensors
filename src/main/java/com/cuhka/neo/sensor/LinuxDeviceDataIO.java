package com.cuhka.neo.sensor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class LinuxDeviceDataIO implements DataIO {
	private final String path;
	private final FileSystem fs;
	
	public LinuxDeviceDataIO(String path) {
		this.path = path;
		this.fs = FileSystems.getDefault();
	}

	@Override
	public void write(String to, String value) {
		Path out = fs.getPath(path, to);
		try {
			try (BufferedWriter writer = Files.newBufferedWriter(out, StandardCharsets.US_ASCII)) {
				writer.write(value);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public String read(String from) {
		Path in = fs.getPath(path, from);
		
		try {
			try (BufferedReader reader = Files.newBufferedReader(in, StandardCharsets.US_ASCII)) {
				return reader.readLine();
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
