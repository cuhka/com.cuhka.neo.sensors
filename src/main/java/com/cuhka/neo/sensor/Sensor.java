package com.cuhka.neo.sensor;

public interface Sensor {
	String getModelId();
	boolean isEnabled();
}
