package com.cuhka.neo.sensor;

public interface Thermometer {
	/**
	 * Reads the next temperature value from the sensor.
	 * @return measured temperature in ℃
	 */
	double readTemperature();
}
