package com.cuhka.neo.sensor.fxas21002;

import java.io.Serializable;

public class AngularRate implements Serializable {
	private static final long serialVersionUID = 673568249096794167L;
	private final int yaw;
	private final int pitch;
	private final int roll;

	public AngularRate(int roll, int pitch, int yaw) {
		this.yaw = yaw;
		this.pitch = pitch;
		this.roll = roll;
	}

	public int getYaw() {
		return yaw;
	}

	public int getPitch() {
		return pitch;
	}

	public int getRoll() {
		return roll;
	}

	@Override
	public String toString() {
		return "AngularRate [yaw=" + yaw + ", pitch=" + pitch + ", roll=" + roll + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pitch;
		result = prime * result + roll;
		result = prime * result + yaw;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AngularRate other = (AngularRate) obj;
		if (pitch != other.pitch)
			return false;
		if (roll != other.roll)
			return false;
		if (yaw != other.yaw)
			return false;
		return true;
	}
}
