package com.cuhka.neo.sensor.fxas21002;

import static java.lang.Integer.parseInt;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.LinuxDeviceDataIO;
import com.cuhka.neo.sensor.Sensor;

public class Gyroscope implements Sensor {
	private final DataIO io;
	
	public Gyroscope() {
		this(new LinuxDeviceDataIO("/sys/class/misc/FreescaleGyroscope"));
	}
	
	public Gyroscope(DataIO io) {
		this.io = io;
	}

	public void setEnabled(boolean enabled) {
		io.write("enable", enabled);
	}

	@Override
	public boolean isEnabled() {
		return io.readBoolean("enable");
	}

	public AngularRate readAngularRate() {
		String[] data = io.read("data").split(",");
		int x = parseInt(data[0]);
		int y = parseInt(data[1]);
		int z = parseInt(data[2]);
		
		return new AngularRate(z, x, y);
	}

	@Override
	public String getModelId() {
		return "fxas21002";
	}
}
