package com.cuhka.neo.sensor.fxos8700cq;

import static java.lang.Integer.parseInt;

import java.util.Objects;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.LinuxDeviceDataIO;
import com.cuhka.neo.sensor.Sensor;

public class Accelerometer implements Sensor {
	private final DataIO io;
	
	public Accelerometer() {
		this(new LinuxDeviceDataIO("/sys/class/misc/FreescaleAccelerometer"));
	}
	
	public Accelerometer(DataIO io) {
		this.io = Objects.requireNonNull(io, "io");
	}

	public void setEnabled(boolean enable) {
		io.write("enable", enable);
	}
	
	@Override
	public boolean isEnabled() {
		return io.readBoolean("enable");
	}

	public Acceleration readAcceleration() {
		String[] data = io.read("data").split(",");
		
		return new Acceleration(parseInt(data[0]), parseInt(data[1]), parseInt(data[2]));
	}

	public void setPollDelay(long delay) {
		io.write("poll_delay", delay);
	}

	public long getPollDelay() {
		return io.readLong("poll_delay");
	}

	@Override
	public String getModelId() {
		return "fxos8700cq";
	}
}
