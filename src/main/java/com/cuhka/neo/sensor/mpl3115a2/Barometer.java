package com.cuhka.neo.sensor.mpl3115a2;

import java.util.Objects;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.LinuxDeviceDataIO;
import com.cuhka.neo.sensor.Sensor;
import com.cuhka.neo.sensor.Thermometer;

public class Barometer implements Sensor, Thermometer {
	private final DataIO io;
	private final double tempScale;
	private final double pressureScale;

	public Barometer() {
		this(new LinuxDeviceDataIO("/sys/class/i2c-dev/i2c-1/device/1-0060/iio:device0"));
	}

	public Barometer(DataIO io) {
		this.io = Objects.requireNonNull(io, "io");
		tempScale = io.readDouble("in_temp_scale");
		pressureScale = 0.25d; // io.readDouble("in_pressure_scale");
	}

	public double getTempScale() {
		return tempScale;
	}

	public double readTempRaw() {
		return io.readDouble("in_temp_raw");
	}

	@Override
	public double readTemperature() {
		return tempScale * readTempRaw();
	}

	public double getPressureScale() {
		return pressureScale;
	}

	public double readPressureRaw() {
		return io.readDouble("in_pressure_raw");
	}

	/**
	 * Read the atmospheric pressure from the sensor.
	 * @return atmospheric pressure in hPa.
	 */
	public double readPressure() {
		return pressureScale * readPressureRaw() / 100d;
	}

	public String getModelId() {
		return io.read("name");
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
