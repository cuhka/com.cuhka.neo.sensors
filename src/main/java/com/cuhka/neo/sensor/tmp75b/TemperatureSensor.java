package com.cuhka.neo.sensor.tmp75b;

import java.util.Objects;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.Sensor;
import com.cuhka.neo.sensor.Thermometer;

public class TemperatureSensor implements Sensor, Thermometer {
	private final DataIO io;
	
	public TemperatureSensor(DataIO io) {
		this.io = Objects.requireNonNull(io, "io");
	}

	@Override
	public String getModelId() {
		return io.read("name");
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public double readTemperatureRaw() {
		return io.readDouble("temp1_input");
	}
	
	@Override
	public double readTemperature() {
		return readTemperatureRaw() / 1_000d;
	}
}
