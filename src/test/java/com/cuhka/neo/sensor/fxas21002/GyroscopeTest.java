package com.cuhka.neo.sensor.fxas21002;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

import com.cuhka.neo.sensor.DataIO;

public class GyroscopeTest {
	private final DataIO io = mock(DataIO.class, Mockito.CALLS_REAL_METHODS);
	private final Gyroscope sensor = new Gyroscope(io);
	
	@Test
	public void enable() {
		sensor.setEnabled(true);
		verify(io, times(1)).write("enable", "1");
	}
	
	@Test
	public void disable() {
		sensor.setEnabled(false);
		verify(io, times(1)).write("enable", "0");
	}
	
	@Test
	public void enabled() {
		when(io.read(eq("enable"))).thenReturn("1");
		assertTrue(sensor.isEnabled());
	}
	
	@Test
	public void disabled() {
		when(io.read("enable")).thenReturn("0");
		assertFalse(sensor.isEnabled());
	}
	
	@Test
	public void angularRate() {
		when(io.read("data")).thenReturn("8,7,-6");
		AngularRate data = sensor.readAngularRate();
		assertEquals(8, data.getYaw());
		assertEquals(7, data.getPitch());
		assertEquals(-6, data.getRoll());
	}

}
