package com.cuhka.neo.sensor.fxos8700cq;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.fxos8700cq.Acceleration;
import com.cuhka.neo.sensor.fxos8700cq.Accelerometer;

public class AccelerometerTest {
	private DataIO io = mock(DataIO.class, Mockito.CALLS_REAL_METHODS);
	private Accelerometer sensor = new Accelerometer(io);
	
	@Test
	public void enable() {
		sensor.setEnabled(true);
		verify(io, times(1)).write("enable", "1");
	}
	
	@Test
	public void disable() {
		sensor.setEnabled(false);
		verify(io, times(1)).write("enable", "0");
	}
	
	@Test
	public void enabled() {
		when(io.read(eq("enable"))).thenReturn("1");
		assertTrue(sensor.isEnabled());
	}
	
	@Test
	public void disabled() {
		when(io.read("enable")).thenReturn("0");
		assertFalse(sensor.isEnabled());
	}
	
	@Test
	public void readAcceleration() {
		when(io.read("data")).thenReturn("-62,-187,1919");
		Acceleration data = sensor.readAcceleration();
		assertEquals(-62, data.getX());
		assertEquals(-187, data.getY());
		assertEquals(1919, data.getZ());
	}
	
	@Test
	public void setPollDelay() {
		sensor.setPollDelay(900L);
		verify(io, times(1)).write("poll_delay", "900");
	}
	
	@Test
	public void getPollDelay() {
		when(io.read("poll_delay")).thenReturn("42");
		assertEquals(42, sensor.getPollDelay());
	}
}
