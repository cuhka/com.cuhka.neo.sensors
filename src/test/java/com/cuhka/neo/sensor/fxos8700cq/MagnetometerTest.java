package com.cuhka.neo.sensor.fxos8700cq;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.fxos8700cq.MagnetoMeter;
import com.cuhka.neo.sensor.fxos8700cq.MagneticField;

public class MagnetometerTest {
	private DataIO io = mock(DataIO.class, Mockito.CALLS_REAL_METHODS);
	private MagnetoMeter sensor = new MagnetoMeter(io);
	
	@Test
	public void enable() {
		sensor.setEnabled(true);
		verify(io, times(1)).write("enable", "1");
	}
	
	@Test
	public void disable() {
		sensor.setEnabled(false);
		verify(io, times(1)).write("enable", "0");
	}
	
	@Test
	public void enabled() {
		when(io.read(eq("enable"))).thenReturn("1");
		assertTrue(sensor.isEnabled());
	}
	
	@Test
	public void disabled() {
		when(io.read("enable")).thenReturn("0");
		assertFalse(sensor.isEnabled());
	}
	
	@Test
	public void readAcceleration() {
		when(io.read("data")).thenReturn("-62,-187,1919");
		MagneticField data = sensor.readMagneticField();
		assertEquals(-62, data.getX());
		assertEquals(-187, data.getY());
		assertEquals(1919, data.getZ());
	}
	
	@Test
	public void setPollDelay() {
		sensor.setPollDelay(900L);
		verify(io, times(1)).write("poll_delay", "900");
	}
	
	@Test
	public void getPollDelay() {
		when(io.read("poll_delay")).thenReturn("42");
		assertEquals(42, sensor.getPollDelay());
	}
}
