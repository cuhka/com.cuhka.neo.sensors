package com.cuhka.neo.sensor.mpl3115a2;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.cuhka.neo.sensor.DataIO;
import com.cuhka.neo.sensor.mpl3115a2.Barometer;

public class BarometerTest {
	private final DataIO io = mock(DataIO.class, Mockito.CALLS_REAL_METHODS);
	private Barometer sensor;
	
	@Before 
	public void sensorScale() {
		when(io.read("in_temp_scale")).thenReturn("0.0625");
		when(io.read("in_pressure_scale")).thenThrow(new AssertionError("should not be used"));
		sensor = new Barometer(io);
	}
	
	@Test
	public void testTempScale() {
		assertEquals(0.0625d, sensor.getTempScale(), 0.00001d);
	}
	
	@Test
	public void testReadTempRaw() {
		when(io.read("in_temp_raw")).thenReturn("189");
		assertEquals(189, sensor.readTempRaw(), 0d);
	}
	
	@Test
	public void testReadTemp() {
		when(io.read("in_temp_raw")).thenReturn("189");
		assertEquals(11.8125d, sensor.readTemperature(), 0.00001d);
	}
	
	@Test
	public void testPressureScale() {
		assertEquals(0.25d, sensor.getPressureScale(), 0.00001d);
	}
	
	@Test
	public void testPressureRaw() {
		when(io.read("in_pressure_raw")).thenReturn("411125");
		assertEquals(411125d, sensor.readPressureRaw(), 0d);
	}
	
	@Test
	public void testReadPressure() {
		when(io.read("in_pressure_raw")).thenReturn("411125");
		assertEquals(1027.8125d, sensor.readPressure(), 0.00001d);
	}
	
}
