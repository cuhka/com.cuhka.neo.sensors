package com.cuhka.neo.sensor.tmp75b;

import org.junit.Test;
import org.mockito.Mockito;

import com.cuhka.neo.sensor.DataIO;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class TemperatureSensorTest {
	private final DataIO io = mock(DataIO.class,Mockito.CALLS_REAL_METHODS);
	private final TemperatureSensor sensor = new TemperatureSensor(io);
	
	@Test
	public void modelId() {
		when(io.read("name")).thenReturn("lm75");
		assertEquals("lm75", sensor.getModelId());
	}
	
	@Test
	public void readTemperatureRaw() {
		when(io.read("temp1_input")).thenReturn("12500");
		assertEquals(12500d, sensor.readTemperatureRaw(), 0.005d);
	}
	@Test
	public void readTemperature() {
		when(io.read("temp1_input")).thenReturn("12500");
		assertEquals(12.5d, sensor.readTemperature(), 0.005d);
	}
}
